package com.test.db;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.test.entity.SimpleGeofence;

public class DAO {

	private DBHelper helper;
	private SQLiteDatabase db;
	
	public DAO(Context context) {
		super();
		helper = new DBHelper(context);
	}

	public void initConnection(){
		db = helper.getWritableDatabase();
	}
	
	public void closeConnection(){
		if(db != null && db.isOpen()){
			db.close();
		}
	}
	
	public long storeGeofence(SimpleGeofence geofence){
		ContentValues cv = new ContentValues();
		cv.put(DBHelper.GEOFENCE_ID, geofence.id);
		cv.put(DBHelper.GEOFENCE_LAT, geofence.lat);
		cv.put(DBHelper.GEOFENCE_LON, geofence.lon);
		cv.put(DBHelper.GEOFENCE_RAD, geofence.radius);
		cv.put(DBHelper.GEOFENCE_EXP, geofence.expires);
		cv.put(DBHelper.GEOFENCE_TRT, geofence.transition);
		cv.put(DBHelper.GEOFENCE_NAME, geofence.name);
		cv.put(DBHelper.GEOFENCE_CREATED, geofence.created);
		return db.insert(DBHelper.GEOFENCES_TABLE, null, cv);
	}
	
	public SimpleGeofence getGeofence(String id){
		String query = "SELECT * FROM " + DBHelper.GEOFENCES_TABLE + " WHERE "+DBHelper.GEOFENCE_ID + "='" + id +"'";
		Cursor cursor = db.rawQuery(query, null);
		SimpleGeofence geofence = null;

		if(cursor.getCount() > 0){
			cursor.moveToFirst();
			geofence = fillGeofenceFromCursor(cursor);
		}
		
		cursor.close();
		return geofence;
	}
	
	public List<SimpleGeofence> getGeofences(){
		String query = "SELECT * FROM " + DBHelper.GEOFENCES_TABLE;
		Cursor cursor = db.rawQuery(query, null);
		List<SimpleGeofence> geofences = new ArrayList<SimpleGeofence>();

		cursor.moveToFirst();
		while(!cursor.isAfterLast()){
			SimpleGeofence geofence = fillGeofenceFromCursor(cursor);
			geofences.add(geofence);
			cursor.moveToNext();
		}
		
		cursor.close();
		return geofences;
	}
	
	public void deleteGeofence(String id){
		String query = "DELETE FROM " + DBHelper.GEOFENCES_TABLE + " WHERE " + DBHelper.GEOFENCE_ID + "='" + id + "'";
		db.execSQL(query);
	}

	public long getFreeGeofenceId(){
		String query = "SELECT max(" + DBHelper.ROWID + ")" + " FROM " + DBHelper.GEOFENCES_TABLE;
		Cursor cursor = db.rawQuery(query, null);
		long id = 1;
		if(cursor.getCount() > 0){
			cursor.moveToFirst();
			id = cursor.getLong(0) + 1;
		}
		cursor.close();
		return id;
	}
	
	private SimpleGeofence fillGeofenceFromCursor(Cursor cursor) {
		SimpleGeofence geofence = new SimpleGeofence();
		geofence.id = cursor.getString(cursor.getColumnIndex(DBHelper.GEOFENCE_ID));
		geofence.lat = cursor.getDouble(cursor.getColumnIndex(DBHelper.GEOFENCE_LAT));
		geofence.lon = cursor.getDouble(cursor.getColumnIndex(DBHelper.GEOFENCE_LON));
		geofence.radius = cursor.getInt(cursor.getColumnIndex(DBHelper.GEOFENCE_RAD));
		geofence.expires = cursor.getLong(cursor.getColumnIndex(DBHelper.GEOFENCE_EXP));
		geofence.transition = cursor.getInt(cursor.getColumnIndex(DBHelper.GEOFENCE_TRT));
		geofence.name = cursor.getString(cursor.getColumnIndex(DBHelper.GEOFENCE_NAME));
		geofence.created = cursor.getLong(cursor.getColumnIndex(DBHelper.GEOFENCE_CREATED));
		return geofence;
	}
	
}
