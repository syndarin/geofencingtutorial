package com.test.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {

	private final static String DB_NAME = "db";	
	private final static int DB_VERSION = 1;
	
	public final static String ROWID = "ROWID";
	
	public final static String GEOFENCES_TABLE = "geofences";
	
	public final static String GEOFENCE_ID = "id";
	public final static String GEOFENCE_LAT = "lat";
	public final static String GEOFENCE_LON = "lon";
	public final static String GEOFENCE_RAD = "rad";
	public final static String GEOFENCE_EXP = "exp";
	public final static String GEOFENCE_TRT = "trt";
	public final static String GEOFENCE_NAME = "name";
	public final static String GEOFENCE_CREATED = "created";
	
	
	public DBHelper(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		createGeofencesTable(db);
	}
	
	private void createGeofencesTable(SQLiteDatabase db){
		String query = "CREATE TABLE " + GEOFENCES_TABLE + "(" + 
				GEOFENCE_ID + " text not null, " + 
				GEOFENCE_LAT + " real not null, " + 
				GEOFENCE_LON + " real not null, " +
				GEOFENCE_RAD + " integer not null, " + 
				GEOFENCE_EXP + " integer not null, " + 
				GEOFENCE_TRT + " integer not null, " +
				GEOFENCE_NAME + " text, " +
				GEOFENCE_CREATED + " integer" +
				")";
		db.execSQL(query);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		
	}

}
