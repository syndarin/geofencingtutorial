package com.test.maptutorial;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.maps.model.LatLng;

public class GeofenceDialog extends DialogFragment{

	private OnGeofenceEditorListener onGeofenceEditorListener;
	
	private Button btnCreate;
	private EditText txtGeofenceName;
	private Spinner spRadius;
	private Spinner spExpiration;
	private RadioGroup rgTransitionType;
	
	private LatLng position;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		getDialog().getWindow().setBackgroundDrawable(getActivity().getResources().getDrawable(R.drawable.new_geofence_background));
		getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		
		View view = inflater.inflate(R.layout.new_geofence_dialog, container);
		
		btnCreate = (Button) view.findViewById(R.id.btnCreateGeofence);
		btnCreate.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				createGeofence();
			}
		});
		
		txtGeofenceName = (EditText) view.findViewById(R.id.txtGeofenceName);
		
		String[] radiusArray = getActivity().getResources().getStringArray(R.array.possible_geofence_radius);
		ArrayAdapter<String> radiusAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, radiusArray);
		radiusAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spRadius = (Spinner) view.findViewById(R.id.spGeofenceRadius);
		spRadius.setAdapter(radiusAdapter);
		spRadius.setSelection(0);
		
		String[] expirationArray = getActivity().getResources().getStringArray(R.array.possible_geofence_expiration);
		ArrayAdapter<String> expirationAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, expirationArray);
		radiusAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spExpiration = (Spinner) view.findViewById(R.id.spGeofenceExpiration);
		spExpiration.setAdapter(expirationAdapter);
		spExpiration.setSelection(0);
		
		rgTransitionType = (RadioGroup) view.findViewById(R.id.rgTransitionType);
		
		return view;
	}
	
	private void createGeofence(){
		if(onGeofenceEditorListener != null){
			collectDataAndSendToListener();
			dismiss();
		} else {
			throw new IllegalArgumentException("No handler for geofence creation! What are you thinking about, dude?");
		}
	}

	private void collectDataAndSendToListener() {
		String name = txtGeofenceName.getText().toString().trim();
		if(name.isEmpty()){
			name = getActivity().getString(R.string.const_geofence_with_no_title);
		}
		
		int radius = Integer.parseInt((String)spRadius.getSelectedItem());
		
		String expirationString = (String) spExpiration.getSelectedItem();
		String neverExpire = getResources().getString(R.string.const_never_expire);
		long expiration = expirationString.equals(neverExpire) ? Geofence.NEVER_EXPIRE : Long.parseLong(expirationString)*3600*1000;
		
		int transitionType = rgTransitionType.getCheckedRadioButtonId() == R.id.rbTransitionEnter ? Geofence.GEOFENCE_TRANSITION_ENTER : Geofence.GEOFENCE_TRANSITION_EXIT;
	
		onGeofenceEditorListener.onGeofenceEditionFinished(getActivity(), position, name, radius, expiration, transitionType);
	}

	public void setPosition(LatLng position) {
		this.position = position;
	}

	public void setOnGeofenceEditorListener(OnGeofenceEditorListener onGeofenceEditorListener) {
		this.onGeofenceEditorListener = onGeofenceEditorListener;
	}

	public interface OnGeofenceEditorListener{
		void onGeofenceEditionFinished(Context context, LatLng pos, String name, int radius, long expiration, int transitionType);
	}
	
}
