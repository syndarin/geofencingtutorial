package com.test.maptutorial;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.bugsense.trace.BugSenseHandler;
import com.flurry.android.FlurryAgent;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.localytics.android.LocalyticsSession;
import com.test.db.DAO;

public class MapTutorialMain extends FragmentActivity implements ServiceConnectionFailedListener{
	
	private static final String TAG_ERROR_DIALOG = "error";
	private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000; 
	
	private LocationAPIManager locationApi;
	
	private LocalyticsSession localytics;
	
	private boolean refreshUserPositionOnMap;
	
	private DAO dao;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        dao = new DAO(this);
        locationApi = new LocationAPIManager(this, dao, this);
        initNavigationBar();
        initLocalytics();
        BugSenseHandler.initAndStartSession(this, "40df2898");
    }
    
	@Override
	protected void onResume() {
		super.onResume();
		dao.initConnection();
		initLocalyticsSession();
		checkGPServicesInstalledAndInitLocationApi();
		BugSenseHandler.startSession(this);
    }
	
	@Override
	protected void onStart() {
		super.onStart();
		FlurryAgent.onStartSession(this, getString(R.string.flurry_app_id));
	}

	@Override
	protected void onStop() {
		locationApi.disposeLocationServices();
		dao.closeConnection();
		closeLocalyticsSession();
		BugSenseHandler.closeSession(this);
		super.onStop();
		FlurryAgent.onEndSession(this);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	if(requestCode == CONNECTION_FAILURE_RESOLUTION_REQUEST){
    		switch(resultCode){
    			case RESULT_OK: launchLocationApi();
    		}
    	}
    }
	
	private void initLocalytics(){
		localytics = new LocalyticsSession(getApplicationContext());
	}
	
	private void initLocalyticsSession(){
		localytics.open();
		localytics.upload();
	}
	
	private void closeLocalyticsSession(){
		localytics.close();
		localytics.upload();
	}
	
	private void initNavigationBar(){
		String[] navigationMenuStrings = getResources().getStringArray(R.array.navigation_menu);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, navigationMenuStrings);
		ListView sideMenu = (ListView)findViewById(R.id.navigationList);
		sideMenu.setAdapter(adapter);
	}
	
	private void checkGPServicesInstalledAndInitLocationApi(){
    	int availabilityCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
    	if(availabilityCode == ConnectionResult.SUCCESS){
    		launchLocationApi();
    	}else{
    		showGooglePlayServicesErrorDialog(availabilityCode);
    	}
    }

	private void launchLocationApi() {
		GoogleMap map = ((SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
		locationApi.setupLocationServices(map);
	}
	
	private void showGooglePlayServicesErrorDialog(int errorCode) {
		Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(errorCode, this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
		if(errorDialog != null){
			showDialog(errorDialog, TAG_ERROR_DIALOG);
		}
	}  
	
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.map_tutorial_main, menu);
        return true;
    }

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		switch(item.getItemId()){
			case R.id.menu_add_geofence_point: 
				localytics.tagEvent("add geofence request");
				locationApi.addGeofenceRequest(getSupportFragmentManager()); break; 
			case R.id.menu_refresh_user_position: changeRefreshUserPositionSettings(item); break;
		}
		return true;
	}

	private void changeRefreshUserPositionSettings(MenuItem item) {
		boolean cameraFollowsUser = !locationApi.isCameraFollowsUser();
		locationApi.setCameraFollowsUser(cameraFollowsUser);
		item.setChecked(refreshUserPositionOnMap);
	}
	
	private void showDialog(Dialog dialog, String tag){
		DialogFragmentWrapper fragment = new DialogFragmentWrapper();
		fragment.setDialog(dialog);
		fragment.show(getSupportFragmentManager(), tag);
	}
	
	@SuppressWarnings("unused")
	private Dialog createProgressDialog(int messageId){
		ProgressDialog progressDialog = new ProgressDialog(this);
		progressDialog.setIndeterminate(true);
		progressDialog.setMessage(getString(messageId));
		return progressDialog; 
	}

	@Override
	public void onServiceConnectionFailed(Services service, ConnectionResult result) {
		if(result.hasResolution()){
			tryToSolveConnectionProblem(result);
		}else{
			showGooglePlayServicesErrorDialog(result.getErrorCode());
		}
	}
	
	private void tryToSolveConnectionProblem(ConnectionResult result) {
		try {
			result.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
		} catch (SendIntentException e) {
			e.printStackTrace();
		}
	}
}
