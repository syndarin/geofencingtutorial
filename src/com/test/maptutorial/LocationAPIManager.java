package com.test.maptutorial;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.test.db.DAO;

public class LocationAPIManager implements ConnectionCallbacks, OnConnectionFailedListener{

	private LocationClient locationClient;
	private LocationRequest locationRequest;
	
	private MapManager mapManager;
	private GeofenceManager geofenceManager;
	private ServiceConnectionFailedListener connectionFailedListener;
	
	private boolean stateConnected;
	
	public LocationAPIManager(Context context, DAO dao, ServiceConnectionFailedListener connectionFailedListener) {
		super();
		this.connectionFailedListener = connectionFailedListener;
		locationClient = new LocationClient(context, this, this);
		locationRequest = LocationRequest.create()
				.setInterval(3000)
				.setFastestInterval(2000)
				.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
		mapManager = new MapManager();
		geofenceManager = new GeofenceManager(dao, locationClient);
		geofenceManager.setMapManager(mapManager);
				
	}

	@Override
	public void onConnected(Bundle arg0) {
		stateConnected = true;
		Location lastLocation = locationClient.getLastLocation();
		if(lastLocation != null){
			mapManager.setLastKnownGeoPosition(new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude()));
		}
		locationClient.requestLocationUpdates(locationRequest, mapManager);
	}
	
	@Override
	public void onDisconnected() {
		stateConnected = false;
		locationClient.removeLocationUpdates(mapManager);
	}
	
	public void setupLocationServices(GoogleMap map){
		locationClient.connect();
		if(!mapManager.isMapInitialized()){
			mapManager.initializeMap(map);
			geofenceManager.loadCurrentGeofences();
		}
	}
	
	public void disposeLocationServices(){
		if(locationClient!=null && locationClient.isConnected()){
			locationClient.removeLocationUpdates(mapManager);
			locationClient.disconnect();
		}
	}
	
	public void addGeofenceRequest(final FragmentManager fManager){
		mapManager.enableGeofenceCreationMode();
		mapManager.setOnMarkerDragListener(new OnMarkerDragListener() {
			
			@Override
			public void onMarkerDragStart(Marker arg0) {
			}
			
			@Override
			public void onMarkerDragEnd(Marker arg0) {
				LatLng newGeofencePosition = mapManager.getEditMarkerPosition();
				mapManager.disableGeofenceCreationMode();
				mapManager.setOnMarkerDragListener(null);
				showGeofenceEditorDialog(fManager, newGeofencePosition);
			}
			
			@Override
			public void onMarkerDrag(Marker arg0) {
			}
		});
	}
	
	private void showGeofenceEditorDialog(FragmentManager fManager, LatLng position){
		GeofenceDialog dialog = new GeofenceDialog();
		dialog.setOnGeofenceEditorListener(geofenceManager);
		dialog.setPosition(position);
		dialog.show(fManager, "geofencedialog");
	}
	
	public void setCameraFollowsUser(boolean cameraFollowsUser){
		mapManager.setCameraFollowsUserLocation(cameraFollowsUser);
	}
	
	public boolean isCameraFollowsUser(){
		return mapManager.isCameraFollowsUserLocation();
	}

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		connectionFailedListener.onServiceConnectionFailed(Services.LOCATION, result);
	}
}
