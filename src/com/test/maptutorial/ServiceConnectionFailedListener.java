package com.test.maptutorial;

import com.google.android.gms.common.ConnectionResult;

public interface ServiceConnectionFailedListener {

	void onServiceConnectionFailed(Services service, ConnectionResult result);
	
}
