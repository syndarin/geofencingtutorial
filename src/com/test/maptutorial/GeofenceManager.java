package com.test.maptutorial;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationClient.OnAddGeofencesResultListener;
import com.google.android.gms.location.LocationStatusCodes;
import com.google.android.gms.maps.model.LatLng;
import com.test.db.DAO;
import com.test.entity.SimpleGeofence;
import com.test.maptutorial.GeofenceDialog.OnGeofenceEditorListener;

public class GeofenceManager implements OnGeofenceEditorListener, OnAddGeofencesResultListener {

	private final String tag = getClass().getSimpleName();

	private DAO dao;
	private LocationClient locationClient;
	private MapManager mapManager;

	public GeofenceManager(DAO dao, LocationClient locationClient) {
		super();
		this.dao = dao;
		this.locationClient = locationClient;
	}
	
	public void loadCurrentGeofences(){
		List<SimpleGeofence> geofences = dao.getGeofences();
		checkForExpiredGeofences(geofences);
		showActiveGeofencesOnMap(geofences);
	}
	
	private void checkForExpiredGeofences(List<SimpleGeofence> geofences){
		for(Iterator<SimpleGeofence> iterator = geofences.iterator(); iterator.hasNext(); ){
			SimpleGeofence geofence = iterator.next();
			if(geofence.expires != Geofence.NEVER_EXPIRE){
				if((geofence.created + geofence.expires) < System.currentTimeMillis()){
					dao.deleteGeofence(geofence.id);
					iterator.remove();
				}
			}
		}
	}

	private void showActiveGeofencesOnMap(List<SimpleGeofence> geofences){
		for(int i = 0, max = geofences.size(); i < max; i++){
			mapManager.showGeofenceAreaOnMap(geofences.get(i));
		}
	}

	@Override
	public void onGeofenceEditionFinished(Context context, LatLng pos, String name, int radius, long expiration, int transitionType) {
		long nextFreeId = dao.getFreeGeofenceId();

		SimpleGeofence geofenceDescription = new SimpleGeofence();
		geofenceDescription.id = Long.valueOf(nextFreeId).toString();
		geofenceDescription.lat = pos.latitude;
		geofenceDescription.lon = pos.longitude;
		geofenceDescription.radius = radius;
		geofenceDescription.expires = expiration;
		geofenceDescription.transition = transitionType;
		geofenceDescription.name = name;
		geofenceDescription.created = System.currentTimeMillis();

		dao.storeGeofence(geofenceDescription);

		//TODO move code to analytics helper
		Map<String, String> params = new HashMap<String, String>();
		params.put("latitude", Double.valueOf(pos.latitude).toString());
		params.put("longitude", Double.valueOf(pos.longitude).toString());
		params.put("expiration", expiration == Geofence.NEVER_EXPIRE ? "NEVER EXPIRE" : Long.valueOf(expiration).toString());
		params.put("transition", transitionType == Geofence.GEOFENCE_TRANSITION_ENTER ? "ENTER" : "EXIT");
		params.put("name", name);
		params.put("radius", Integer.valueOf(radius).toString());
		
		FlurryAgent.logEvent("Geofence created", params);

		registerGeofenceInLocationClient(context, geofenceDescription);
	}

	private void registerGeofenceInLocationClient(Context context, SimpleGeofence geofenceDescription) {
		Geofence geofence = geofenceDescription.toGeofence();
		List<Geofence> geofenceList = Collections.singletonList(geofence);
		locationClient.addGeofences(geofenceList, createTransitionPendingIntent(context), this);
	}
	
    private PendingIntent createTransitionPendingIntent(Context context){
    	Intent i = new Intent(context, ReceiveTransitionsIntentService.class);
    	return PendingIntent.getService(context, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);
    }

	@Override
	public void onAddGeofencesResult(int statusCode, String[] geofenceRequestIds) {
		if(statusCode == LocationStatusCodes.SUCCESS){
			for(int i = 0, max = geofenceRequestIds.length; i < max; i++){
				SimpleGeofence geofence = dao.getGeofence(geofenceRequestIds[i]);
				mapManager.showGeofenceAreaOnMap(geofence);
			}
		}else{
			Log.d(tag, "Add geofences failed!");
		}
	}

	public void setMapManager(MapManager mapManager) {
		this.mapManager = mapManager;
	}

}
