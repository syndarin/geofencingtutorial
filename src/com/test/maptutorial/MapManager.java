package com.test.maptutorial;

import android.location.Location;

import com.google.android.gms.location.LocationListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.test.entity.SimpleGeofence;

public class MapManager implements LocationListener{
	
	private final int DEFAULT_ZOOM_LEVEL = 18;

	private GoogleMap map;

	private Marker currentLocationMarker;
	
	private LatLng currentGeoPosition = new LatLng(0, 0);
	
	private boolean cameraFollowsUserLocation;

	private Marker editGeofenceMarker;

	@Override
	public void onLocationChanged(Location location) {
		currentGeoPosition = new LatLng(location.getLatitude(), location.getLongitude());
		refreshMapView();
	}

	private void refreshMapView() {
		setUserMarkerToCurrentLocation();
		if(cameraFollowsUserLocation){
			moveCameraToLocation(currentGeoPosition);
		}
	}
	
	private void setUserMarkerToCurrentLocation(){
		currentLocationMarker.setPosition(currentGeoPosition);
	}
	
	private void moveCameraToLocation(LatLng position){
		map.moveCamera(CameraUpdateFactory.newLatLng(position));
	}
	
	public boolean isMapInitialized(){
		return map != null;
	}

	public void initializeMap(GoogleMap googleMap) {
		map = googleMap;
		map.moveCamera(CameraUpdateFactory.zoomTo(DEFAULT_ZOOM_LEVEL));
		currentLocationMarker = map.addMarker(
				new MarkerOptions()
				.position(currentGeoPosition)
				.anchor(0.5f, 0.5f)
				.draggable(false)
				.icon(BitmapDescriptorFactory.fromResource(R.drawable.foot))
				.snippet("Some snippet")
		);
	}
	
	public void setLastKnownGeoPosition(LatLng position){
		if(position != null){
			currentGeoPosition = position;
			setUserMarkerToCurrentLocation();
			moveCameraToLocation(currentGeoPosition);
		}
	}
	
	public void showGeofenceAreaOnMap(SimpleGeofence geofence){
		LatLng position = new LatLng(geofence.lat, geofence.lon);
		map.addCircle(new CircleOptions().center(position).fillColor(0x5500ff00).radius(geofence.radius).strokeWidth(1));
	}
	
	public void setOnMarkerDragListener(OnMarkerDragListener dragListener){
		map.setOnMarkerDragListener(dragListener);
	}
	
	public void enableGeofenceCreationMode(){
		currentLocationMarker.setVisible(false);
		LatLng cameraCenter = map.getCameraPosition().target;
		editGeofenceMarker = map.addMarker(new MarkerOptions().anchor(0.5f, 1).position(cameraCenter).draggable(true).title("Drag me to set new point of interest").icon(BitmapDescriptorFactory.fromResource(R.drawable.arrow)));
		editGeofenceMarker.showInfoWindow();
	}
	
	public void disableGeofenceCreationMode(){
		currentLocationMarker.setVisible(true);
		editGeofenceMarker.remove();
	}
	
	public LatLng getEditMarkerPosition(){
		return editGeofenceMarker.getPosition();
	}

	public boolean isCameraFollowsUserLocation() {
		return cameraFollowsUserLocation;
	}

	public void setCameraFollowsUserLocation(boolean cameraFollowsUserLocation) {
		this.cameraFollowsUserLocation = cameraFollowsUserLocation;
	}
	
}
