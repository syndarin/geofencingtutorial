package com.test.maptutorial;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

public class DialogFragmentWrapper extends DialogFragment {

	private Dialog dialog;

	public void setDialog(Dialog dialog){
		this.dialog = dialog;
	}
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		return dialog;
	}
	
	public void disposeDialog(){
		getDialog().dismiss();
	}
	
}
