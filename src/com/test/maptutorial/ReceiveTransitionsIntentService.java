package com.test.maptutorial;

import java.util.List;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.LocationClient;
import com.test.db.DAO;
import com.test.entity.SimpleGeofence;

public class ReceiveTransitionsIntentService extends IntentService {

	private final static String tag = ReceiveTransitionsIntentService.class.getSimpleName();
	
	public ReceiveTransitionsIntentService() {
		super("ReceiveTransitionsIntentService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		if(LocationClient.hasError(intent)){
			int errorCode = LocationClient.getErrorCode(intent);
			Log.e(tag, "Location Service Error - "+errorCode);
		}else{
			int transitionType = LocationClient.getGeofenceTransition(intent);
			if(transitionType == Geofence.GEOFENCE_TRANSITION_ENTER || transitionType == Geofence.GEOFENCE_TRANSITION_EXIT){
				List<Geofence> triggeredGeofences = LocationClient.getTriggeringGeofences(intent);
				if(triggeredGeofences != null){
					DAO dao = new DAO(this);
					dao.initConnection();
					for(Geofence gf : triggeredGeofences){
						SimpleGeofence geofenceDescription = dao.getGeofence(gf.getRequestId());
						createNotification(getTransitionText(transitionType), geofenceDescription.name);
					}
					dao.closeConnection();
				}
			}else{
				Log.e(tag, "Illegal transition type - "+transitionType);
			}
		}
	}
	
	private String getTransitionText(int transition){
		return transition == Geofence.GEOFENCE_TRANSITION_ENTER ? "entered" : "leaved";
	}
	
	private void createNotification(String transition, String pointName){
		
		Intent intent = new Intent(this, MapTutorialMain.class);
		PendingIntent pi = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		
		String message = "You've just "+ transition + pointName;
		
		NotificationManager nManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		
		NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
			.setContentTitle("Big Brother")
			.setVibrate(new long[]{500, 0, 500, 0, 500})
			.setPriority(NotificationCompat.PRIORITY_HIGH)
			.setContentText(message)
			.setContentIntent(pi)
			.setSmallIcon(R.drawable.ok)
			.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.ok))
			.setSubText("sub text")
			.addAction(R.drawable.add, "action title", pi);
		
		Notification n = new NotificationCompat.BigTextStyle(builder)
			.bigText(message)
			.setBigContentTitle("Some BIG content title")
			.setSummaryText("some summary text")
			.build();
		
		
		nManager.notify(0, n);
	}

}
