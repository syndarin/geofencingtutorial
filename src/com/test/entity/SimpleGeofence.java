package com.test.entity;

import com.google.android.gms.location.Geofence;

public class SimpleGeofence {

	public String id;
	public double lat;
	public double lon;
	public int radius;
	public long expires;
	public int transition;
	public String name;
	public long created;
	
	public SimpleGeofence() {
		super();
	}

	public Geofence toGeofence(){
		return new Geofence.Builder()
			.setRequestId(id)
			.setCircularRegion(lat, lon, radius)
			.setExpirationDuration(expires)
			.setTransitionTypes(transition)
			.build();
	}

	@Override
	public String toString() {
		return "SimpleGeofence [id=" + id + ", lat=" + lat + ", lon=" + lon + ", radius=" + radius + ", expires=" + expires + ", transition=" + transition + ", name=" + name + "]";
	}
	
}
